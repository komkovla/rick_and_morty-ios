# Rick_and_Morty-IOS

Solution for the [Rich and Morty test task](https://github.com/AckeeCZ/ios-task-rick-and-morty)

Latest version  -> `master`

Please check out **issues** for several things I couldn't solve in the given time

design guideLines from abstract are just a guideLines?(there is no way to downdload anything rather then jpeg)

On `main` branch is a modified version of the task with local character search and fetching details from `all characters` API request. Favorite function not implemented
since this version of solution is not a prioritized

Tested on *iphone 13 mini* and *ipad Air 3*