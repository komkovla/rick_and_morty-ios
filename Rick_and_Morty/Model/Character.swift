//
//  Charackter.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 14.07.2022.
//

import Foundation

struct Character : Identifiable{
    let id      : Int
    let name    : String
    let status  : String
    let image   : URL
    var isFavorite : Bool = false
}

extension Character : Codable{
    enum CodingKeys : String, CodingKey{
        case id
        case name
        case status
        case image
    }
    
    init(from decoder : Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        status = try container.decode(String.self, forKey: .status)
        image = try container.decode(URL.self, forKey: .image)
    }
}

struct CharacterExt : Codable{
    
    struct Origin : Codable{
        let name : String
    }
    struct Location : Codable{
        let name : String
    }
    let species : String
    let type    : String
    let gender  : String
    let origin  : Origin
    let location: Location
}


let dummyCharExt : CharacterExt = .init(
                                   species: "Human",
                                   type: "",
                                   gender: "Male",
                                   origin: CharacterExt.Origin(name: "Earth"),
                                   location: CharacterExt.Location(name: "Earth")
                                   )
let dummyChar : Character = .init(id: 1,
                                  name: "Rick",
                                  status: "Alive",
                                  image: URL(string: "https://rickandmortyapi.com/api/character/avatar/1.jpeg")!)

