//
//  ViewModel.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 14.07.2022.
//

import Foundation
import SwiftUI




class CharacterViewModel : ObservableObject{
    @Published private(set) var model : Model = .init()
    @Published private(set) var charInfo : Optional<CharacterExt> = nil
    @AppStorage("favorites") var favorites: Set<Int> =  []
    init(){
        fetchAllCharacters()
        print(favorites)
        
    }
    
    func clearFilter(){
        model.filteredCharacters.removeAll()
    }
    
    func fetchAllCharacters(){
        model.characters.removeAll()
        fetchCharactersFromURL(url: URL(string: Common.APIUrl)!)
    }
    func fetchFilteredCharacters(filter: String) -> Bool{
        clearFilter()
        let filterFormated = filter.replacingOccurrences(of: " ", with: "%20")
        let request : String = Common.APINameFilterUrl + filterFormated
        guard let requestURL = URL(string: request )
        else{
            return false
        }
        fetchFilteredCharactersFromURL(url: requestURL)
        return true
    }
    func setFavorite(_ character : Character, isFav : Bool){
        if isFav{
            favorites.insert(Int(character.id))
        }
        else{
            favorites.remove(Int(character.id))
        }
    }
    
    func getInfo(_ character: Character){
        let url = URL(string: Common.APICharUrl + String(character.id))
        fetchSingleCharacterFromURL(url: url)
    }
    
    func loadFavorite(id: Int) -> Bool{
        favorites.contains(id)
    }
    
    func getFavs() -> [Character]{
        model.characters.filter { favorites.contains($0.id)}
    }
    
    struct PageInfo : Codable{
        let count : Int
        let pages : Int
        let next : Optional<URL>
        let prev : Optional<URL>
    }
    struct CharactersResponse : Decodable{
        let info : PageInfo
        let results : [Character]
    }
    
    func fetchCharactersFromURL(url : Optional<URL>){
        guard (url != nil)
        else{
            
            return
        }
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) {
            [weak self]
            data, response, error in
            if let error = error{
                print(error)
                return
            }
            if let data = data{
                let rawResponce : CharactersResponse = try! JSONDecoder().decode(CharactersResponse.self, from: data)
                DispatchQueue.main.async {
                    self?.model.characters.append(contentsOf: rawResponce.results)
                }
                //Recursion
                self?.fetchCharactersFromURL(url : rawResponce.info.next)
            }
        }
        task.resume()
    }
    
    func fetchFilteredCharactersFromURL(url : Optional<URL>){
        guard (url != nil)
        else{
            return
        }
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) {
            [weak self]
            data, response, error in
            if let error = error{
                print(error)
                return
            }
            
            if let data = data{
                var rawResponce : Optional<CharactersResponse>
                do{
                    rawResponce = try JSONDecoder().decode(CharactersResponse.self, from: data)
                }
                catch{
                    rawResponce = nil
                    return
                }
                DispatchQueue.main.async {
                    self?.model.filteredCharacters.append(contentsOf: ((rawResponce!.results)))
                }
                //Recursion
                self?.fetchFilteredCharactersFromURL(url : rawResponce!.info.next)
            }
        }
        task.resume()
    }
    
    func fetchSingleCharacterFromURL(url : Optional<URL>){
        var request = URLRequest(url:url!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) {
            [weak self]
            data, response, error in
            if let error = error{
                print(error)
                return
            }
            if let data = data{
                DispatchQueue.main.async {
                    self?.charInfo =  try! JSONDecoder().decode(CharacterExt.self, from: data)
                }
            }
        }
        task.resume()
    }
    
}

