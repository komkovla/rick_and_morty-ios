//
//  remoteImage.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 14.07.2022.
//

import SwiftUI

struct remoteImage: View {
    @State private var image: Image?
    let url: URL
    
    var body: some View {
        if let image = image {
            image
                .resizable()
        }
        else {
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
                .onAppear{
                    fetchImage()
                }
        }
    }
    
    private func fetchImage() {
        DispatchQueue.global().async {
            
            let data = try! Data(contentsOf: url)
            let uiImage = UIImage(data: data)!
            
            DispatchQueue.main.async{
                image = Image(uiImage: uiImage)
            }
            
        }
    }
}

struct remoteImage_Previews: PreviewProvider {
    static var previews: some View {
        remoteImage(url: URL(string: "https://placeimg.com/320/320/nature")!)
    }
}
