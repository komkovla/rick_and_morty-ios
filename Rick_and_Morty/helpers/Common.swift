//
//  Common.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 17.07.2022.
//

import Foundation


enum Common{
    static let APIUrl = "https://rickandmortyapi.com/api/character"
    static let APINameFilterUrl = "https://rickandmortyapi.com/api/character/?name="
    static let APICharUrl = "https://rickandmortyapi.com/api/character/"
    

}
