//
//  FavoriteButton.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 15.07.2022.
//

import SwiftUI

struct FavoriteButton: View {
    @Binding var isSet : Bool
    var body: some View {
        Button(action: {isSet.toggle()}){
            Image(systemName: isSet ? "star.fill" : "star")
                .foregroundColor(isSet ? .blue : .gray)
        }
    }
}

struct FavoriteButton_Previews: PreviewProvider {
    static var previews: some View {
        FavoriteButton(isSet: .constant(true))
    }
}
