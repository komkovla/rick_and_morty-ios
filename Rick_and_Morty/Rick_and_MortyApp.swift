//
//  Rick_and_MortyApp.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 13.07.2022.
//

import SwiftUI

@main
struct Rick_and_MortyApp: App {
    @StateObject var characterViewModel: CharacterViewModel = .init()
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(characterViewModel)
        }
    }
}
