//
//  CharacktersView.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 13.07.2022.
//

import SwiftUI



struct feedView: View{
    @Environment(\.isSearching) var isSearching
    @EnvironmentObject var characterViewModel : CharacterViewModel
    var body: some View{
        if isSearching == false{
            LazyVGrid(columns: [GridItem()]){
                ForEach(characterViewModel.model.characters, id: \.id) { character in
                    CharacterView(character: character)
                }
            }
        }
        else{
            LazyVGrid(columns: [GridItem()]){
                ForEach(characterViewModel.model.filteredCharacters, id: \.id) { character in
                    CharacterFilterView(character: character)
                }
            }
            .onDisappear(){
                //characterViewModel.clearFilter()
            }
        }
        NavigationLink(destination: EmptyView()) {
            EmptyView()
        }
    }
}


struct FeedViewSearchable: View {
    @State private var searchText = ""
    
    @State private var isFilterError = false
    @ObservedObject var characterViewModel : CharacterViewModel
    var body: some View {
        ScrollView{
            feedView()
        }
        .searchable(text: $searchText)
        .onSubmit(of: .search){
            if !characterViewModel.fetchFilteredCharacters(filter: searchText){
                isFilterError = true
                searchText = ""
            }
        }
        
        .alert("Filter Value not Supported", isPresented: $isFilterError){
            Button("OK", role: .cancel) { }
            
        }
    }
}

struct CharacktersView_Previews: PreviewProvider {
    static var previews: some View {
        ForEach(ColorScheme.allCases, id: \.self) {
            FeedViewSearchable(characterViewModel: CharacterViewModel())
                .environmentObject(CharacterViewModel())
                .preferredColorScheme($0)
        }
        
        
    }
}
