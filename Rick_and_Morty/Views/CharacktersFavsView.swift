//
//  CharacktersFavsView.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 15.07.2022.
//

import SwiftUI

struct CharacktersFavsView: View {
    @ObservedObject var viewModel : CharacterViewModel
    var body: some View {
        ScrollView{
            LazyVGrid(columns: [GridItem()]){
                ForEach(viewModel.getFavs(), id: \.id) { character in
                        CharacterView(character: character)
                    }
            }
        }
    }
}

struct CharacktersFavsView_Previews: PreviewProvider {
    static var previews: some View {
        CharacktersFavsView(viewModel: CharacterViewModel())
            
    }
}
