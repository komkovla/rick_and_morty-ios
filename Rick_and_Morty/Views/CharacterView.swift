//
//  CharackterView.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 13.07.2022.
//

import SwiftUI





struct CharInfoView: View{
    var character : Character
    @EnvironmentObject var characterViewModel: CharacterViewModel
    var body: some View{
        HStack{
            remoteImage(url: character.image)
                .aspectRatio(1, contentMode: .fit)
                .clipShape(RoundedRectangle(cornerRadius: 10))
                .padding(10)
                
            
            VStack(alignment: .leading, spacing: 0){
                HStack{
                    Text(character.name)
                        .foregroundColor(.primary)
                    if(character.isFavorite){
                        Image(systemName: "star.fill")
                            .foregroundColor(.blue)
                    }
                }
                Text(character.status)
                    .foregroundColor(.secondary)
            }
            .padding(.vertical)
                Spacer()
        }
        }
        
    }



struct CharacterView: View {
    let screenSize = UIScreen.main.bounds
    @Environment(\.colorScheme) var colorScheme
    
    @EnvironmentObject var characterViewModel: CharacterViewModel
    @State private var image: Image?
    @State var character : Character
    var body: some View {
        ZStack{
            RoundedRectangle(cornerRadius: 20)
                .foregroundColor(colorScheme == .light ? Color(.white) : Color(.systemGray4))
                .aspectRatio(344/61, contentMode: .fit)
                .overlay(){
                    HStack{
                        CharInfoView(character: character)
                        Spacer()
                        NavigationLink(destination: CharackterDetailView(character: $character)){
                                Image(systemName: "chevron.forward")
                                .padding()
                                .foregroundColor(.secondary)
                        }
                    }
                    
                }
                .padding(.horizontal)
                .onAppear(){
                    character.isFavorite =  characterViewModel.loadFavorite(id: character.id)
                }
        
    }
        
}
}

struct CharacterFilterView: View {
    let screenSize = UIScreen.main.bounds
    @Environment(\.colorScheme) var colorScheme
    
    @EnvironmentObject var characterViewModel: CharacterViewModel
    var characterIndex: Int {
            characterViewModel.model.characters.firstIndex(where: {$0.id == character.id})!
        }
    
    @State private var image: Image?
    @State var character : Character
    var body: some View {
            HStack{
                        CharInfoView(character: character)
                        Spacer()
                        NavigationLink(destination: CharackterDetailView(character: $character)){
                                Image(systemName: "chevron.forward")
                                .padding()
                                .foregroundColor(.secondary)
                        }
                        
                    }
            .aspectRatio(344/61 ,contentMode: .fit)
            .padding(.horizontal)
            .onAppear(){
                character.isFavorite =  characterViewModel.loadFavorite(id: character.id)
            }
            
}
}


struct CharackterView_Previews: PreviewProvider {
    static var previews: some View {
        CharacterFilterView(character: Character(id: 1,
                                              name: "Rick Sanches",
                                              status: "Alive",
                                              image: URL(string: "https://rickandmortyapi.com/api/character/avatar/1.jpeg")!,
                                              isFavorite: true
                                             )
                       )
        .environmentObject(CharacterViewModel())
        .background(Color(UIColor.systemGroupedBackground))
        .preferredColorScheme(.light)
        CharacterView(character: Character(id: 1,
                                              name: "Rick Sanches",
                                              status: "Alive",
                                              image: URL(string: "https://rickandmortyapi.com/api/character/avatar/1.jpeg")!,
                                              isFavorite: true
                                             )
                       )
        .environmentObject(CharacterViewModel())
        .background(Color(UIColor.systemGroupedBackground))
        .preferredColorScheme(.light)
        
    }
}
