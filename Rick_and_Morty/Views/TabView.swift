//
//  TabView.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 13.07.2022.
//

import SwiftUI


struct TabView: View {
    @Binding var isFavorite: Bool
    let screenSize = UIScreen.main.bounds
    @Environment(\.colorScheme) var colorScheme : ColorScheme
    var body: some View {
        HStack{
            Button(action: {
                withAnimation(){
                    isFavorite = false
                }
                
            }){
                Text("Rick")
                    .foregroundColor(isFavorite ? .secondary : .blue)
            }
            Spacer()
            Button(action: {
                withAnimation(){
                    isFavorite = true
                }
                
            }){
                Text("Star")
                    .foregroundColor(isFavorite ? .blue : .secondary)
            }
        }
        .padding()
        
            .padding()
                    .frame(width: screenSize.width * 0.5, height: screenSize.height * 0.1)
                    .background(colorScheme == .light ? Color(.white) : Color(.systemGray4))
                    .clipShape(RoundedRectangle(cornerRadius: 50.0, style: .continuous))
                    .shadow(color: colorScheme == .light ? .gray : .black, radius: 10)
    }
}

struct TabViewOffset: View{
    @Binding var isFavorite: Bool
    var body: some View{
        VStack{
            Spacer()
            TabView(isFavorite: $isFavorite)
        }
        .padding()
    }
}

struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        TabViewOffset( isFavorite: .constant(true)).preferredColorScheme(.dark)
    }
}
