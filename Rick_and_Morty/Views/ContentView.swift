//
//  ContentView.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 13.07.2022.
//

import SwiftUI

struct ContentView: View {
    @State var isFavoriteOnly = false
    @EnvironmentObject var characterViewModel : CharacterViewModel
    var body: some View {
        NavigationView{
            ZStack{
                if isFavoriteOnly{
                    CharacktersFavsView(viewModel: characterViewModel)
                } else {
                    FeedViewSearchable( characterViewModel: characterViewModel)
                }
            }
            .transition(.slide)
            
            .overlay(){
                TabViewOffset(isFavorite: $isFavoriteOnly)
                
            }
                
            .background(Color(.systemGray5))
            .navigationTitle(isFavoriteOnly ? "Favorites" : "Characters" )
        }
        .navigationViewStyle(StackNavigationViewStyle())
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        
        ContentView()
            .environmentObject(CharacterViewModel())
            .preferredColorScheme(.light)
        ContentView()
            .environmentObject(CharacterViewModel())
            .preferredColorScheme(.dark)
    }
}
