//
//  CharackterDetailView.swift
//  Rick_and_Morty
//
//  Created by vlad komkow on 14.07.2022.
//

import SwiftUI


struct CharackterDetailView: View {
    @EnvironmentObject var characterViewModel : CharacterViewModel
    @Environment(\.colorScheme) var colorScheme : ColorScheme
    @Binding var character : Character
    //@Binding var isFavorite: Bool
    var body: some View{
        
        RoundedRectangle(cornerRadius: 20)
            .foregroundColor(colorScheme == .light ? Color(.white) : Color(.systemGray4))
            .overlay(){
                
                VStack(alignment: .leading, spacing: 10){
                    HStack(alignment: .top, spacing: 10){
                        remoteImage(url: character.image)
                            .aspectRatio(1, contentMode: .fit)
                            .clipShape(RoundedRectangle(cornerRadius: 10))
                            .padding(10)
                        
                        VStack(alignment: .leading, spacing: 0){
                            Text("name")
                                .foregroundColor(.secondary)
                                .padding(.vertical)
                            Text(character.name)
                                .foregroundColor(.primary)
                        }
                        FavoriteButton(isSet: $character.isFavorite)
                            .padding()
                            .onChange(of: character.isFavorite){ newValue in
                                characterViewModel.setFavorite(character,isFav: newValue)
                            }
                    }
                    .padding()
                    Divider()
                    infoLine(title: "Status", value: character.status)
                    infoLine(title: "Spicies", value: characterViewModel.charInfo?.species ?? "-")
                    infoLine(title: "Type", value: characterViewModel.charInfo?.type ?? "-")
                    infoLine(title: "Gender", value: characterViewModel.charInfo?.gender ?? "-")
                    infoLine(title: "Origin", value: characterViewModel.charInfo?.origin.name ?? "-")
                    infoLine(title: "Location", value: characterViewModel.charInfo?.location.name ?? "-")
                    Spacer()
                }
                .onAppear(){
                    characterViewModel.getInfo(character)
                }
            }
            .padding()
            .background(Color(.systemGray5))
            
    }
    
    
    func infoLine(title: String, value: String) -> some View{
        HStack{
            Text(title)
                .foregroundColor(.secondary)
            if value.isEmpty{
                Text("-")
                    .foregroundColor(.primary)
            }
            else{
                Text(value)
                    .foregroundColor(.primary)
            }
        }
        .padding()
    }
}

struct CharackterDetailView_Previews: PreviewProvider {
    static var previews: some View {
       
        CharackterDetailView(character: .constant(dummyChar))
            .background(Color(UIColor.systemGroupedBackground))
    }
}
